<?php

session_start();


if (isset($_SESSION['error'])) {
	$error = array();
	$error = $_SESSION['error'];
	var_dump($_SESSION['error']);
}
unset($_SESSION['error']);

?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>Address Book</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
</head>
<body class="add">
	
<div class="container">
	<h1 class="page-header">Address Book</h1>
	<div class="row">
		<?php include_once('./include/sidebar.php'); ?>
		<div class="span9">

			<form action="after.php" method="post">
				<legend>住所を登録する</legend>
				<div class="control-group">
					<label class="control-label" for="inputEmail">名前: </label>
					<div class="controls">
               			<input type="text" name="name" placeholder="名前" class="input-xlarge">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">ふりがな: </label>
					<div class="controls">
               			<input type="text" name="kana" placeholder="ふりがな" class="input-xlarge">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">電話番号: </label>
					<div class="controls">
               			<input type="text" name="phone" placeholder="電話番号" class="input-xlarge">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">メールアドレス: </label>
					<div class="controls">
               			<input type="text" name="email-1" class="input-large">
               			@
               			<input type="text" name="email-2" class="input-xlarge">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">住所: </label>
					<div class="controls">
               			<input type="text" name="pref" class="input-large" placeholder="県名">
              		</div>
              		<div class="controls">
               			<input type="text" name="address" class="input-xxlarge" placeholder="以降の住所">
              		</div>
				</div>
				<input class="btn btn-large btn-primary" type="submit" value="登録">
			</form>
		</div><!-- /.span9 -->
	</div><!-- /.row -->
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>