<?php

require_once('./pdo.php');
require_once('./functions.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id'])) {
	edit();
}
elseif($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['id'])) {
	update();
}
else {
	return_top();
}


