<?php

	$url = $_SERVER['REQUEST_URI'];
	$target = '';

	if (check_url('add.php')) {
		$target = 'add';
	}
	else if (check_url('edit.php')) {
		$target = 'edit';
	}
	else if (check_url('index.php') || strlen(dirname($url) <= 1)) {
		$target = 'index';
	}

function check_url($str = '') {
	if (strlen($str) > 0) {
		if (strpos($GLOBALS["url"], $str) > 0) {
			//return str_replace('.php', '', $str);
			return true;
		}
	}
	else {
		return false;
	}
}

?>

<div class="span3">
	<ul class="nav nav-list bs-docs-sidenav affix">

		<?php if($target === 'index'): ?>
			<li class="active"><a href="./">トップページ <i class="icon-chevron-right"></i></a></li>
		<?php else: ?>
			<li><a href="./">トップページ <i class="icon-chevron-right"></i></a></li>
		<?php endif; ?>

		<?php if($target === 'add'): ?>
			<li class="active"><a href="./add.php">追加 <i class="icon-chevron-right"></i></a></li>
		<?php else: ?>
			<li><a href="./add.php">追加 <i class="icon-chevron-right"></i></a></li>
		<?php endif; ?>
		
	</ul>
</div><!-- /.span3 -->










