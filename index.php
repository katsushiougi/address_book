<?php

require_once('./pdo.php');
require_once('./functions.php');

$data = get_all_data();

?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>Address Book</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
</head>
<body class="index">
	
<div class="container">
	<h1 class="page-header">Address Book</h1>
	<div class="row">
		<?php include_once('./include/sidebar.php'); ?>
		<div class="span9">
			<?php $len = count($data); for($i = 0; $i < $len; $i++): ?>
				<form action="delete.php" method="post">
					<input type="hidden" name="id" value="<?php echo $data[$i]['id']; ?>">
					<table class="table table-bordered">
						<tr>
							<th>名前: </th>
							<td><?php echo $data[$i]['name']; ?></td>
						</tr>
						<tr>
							<th>ふりがな: </th>
							<td><?php echo $data[$i]['kana']; ?></td>
						</tr>
						<tr>
							<th>電話番号: </th>
							<td><?php echo $data[$i]['phone']; ?></td>
						</tr>
						<tr>
							<th>メールアドレス: </th>
							<td><?php echo $data[$i]['email']; ?></td>
						</tr>
						<tr>
							<th>住所: </th>
							<td><?php echo $data[$i]['pref'].$data[$i]['address']; ?></td>
						</tr>
						<tr>
							<td colspan="2" class="mod-delate">
								<div class="pull-right">
									<a href="./edit.php?id=<?php echo $data[$i]['id']; ?>" class="btn-inverse btn">編集</a>
									&nbsp;
									<input class="btn btn-danger" type="submit" value="削除">
								</div>	
							</td>
						</tr>
					</table>
				</form>
			<?php endfor; ?>

		</div><!-- /.span9 -->
	</div><!-- /.row -->
<!-- 	<div class="pagination">
	  <ul>
	    <li><a href="#">Prev</a></li>
	    <li><a href="#">1</a></li>
	    <li><a href="#">2</a></li>
	    <li><a href="#">3</a></li>
	    <li><a href="#">4</a></li>
	    <li><a href="#">Next</a></li>
	  </ul>
	</div> -->
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>