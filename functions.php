<?php

$html_header = <<< _HTML_
<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>Address Book</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
</head>
_HTML_;

$html_footer = <<< _HTML_
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
_HTML_;




//一覧を返す
function get_all_data() {
	try {
		$sql = 'SELECT * FROM my_book ORDER BY id DESC';
		$stmt = $GLOBALS['pdo'] -> query($sql);
		$data = array();

		while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			$data[] = $row;
		}

		return $data;

	} catch (Exception $e) {
		var_dump($e -> getMessage());
	}

	$GLOBALS['pdo'] = null;
}

//DBに登録
function set_db() {
	if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['name'])) {
		return_top();
		return;
	}
	$name =  htmlspecialchars($_POST['name'], ENT_QUOTES);
	$kana = htmlspecialchars($_POST['kana'], ENT_QUOTES);
	$phone = str_replace('-', '', $_POST['phone']);
	$phone = htmlspecialchars($phone, ENT_QUOTES);
	if (strlen($_POST['email-1']) > 0 && strlen($_POST['email-2']) > 0) {
		$email = $_POST['email-1'].'@'.$_POST['email-2'];
	}
	else {
		$email = '';
	}
	$email = htmlspecialchars($email, ENT_QUOTES);

	$pref = htmlspecialchars($_POST['pref'], ENT_QUOTES);
	$address = htmlspecialchars($_POST['address'], ENT_QUOTES);

	// //バリデート
	$error = array();
	if ($name === '') {
		$error[] = '名前を入力してください';
	}
	if ($phone === '') {
		$error[] = '電話番号を入力してください';
	}
	if (count($error) > 0) {
		$_SESSION['error'] = $error;
		header('location:./add.php');
		return;
	}

	try {
		//新規投稿
		$sql = "INSERT INTO my_book "
				."(created, name, kana, phone, email, pref, address)"
				."VALUES"
				."(NOW(), '{$name}', '{$kana}', '{$phone}', '{$email}', '{$pref}', '{$address}')";

		//SQL文を実行
		$stmt = $GLOBALS['pdo'] -> query($sql);

		if($stmt === FALSE) {
			var_dump('INSERT ERROR!');
		}

	} catch (Exception $e) {
		var_dump($e -> getMessage());
	}

	$pod = null;
}

//DBから削除
function delete() {
	try {
		$sql = "DELETE FROM my_book WHERE id = '{$_POST['id']}'";
		$result = $GLOBALS['pdo'] -> query($sql);
		
		if($result === FALSE) {
			var_dump('削除失敗');
		}
	} catch (Exception $e) {
		var_dump($e -> getMessage());
	}

	$pod = null;
}

function edit() {
	$data = array();
	$email_arr = array();

	try {

		$sql = "SELECT * FROM my_book WHERE id = '{$_GET['id']}'";
		$stmt = $GLOBALS['pdo'] -> query($sql);

		if ($stmt -> rowCount() == 1) {
			$GLOBALS['data'] = $stmt -> fetch(PDO::FETCH_ASSOC);
			$GLOBALS['email_arr'] = explode('@', $GLOBALS['data']['email']);
			publish_edit_html();
		}
		else {
			return_top();
		}		

	} catch (Exception $e) {
		var_dump($e -> getMessage());
	}
	$pdo = null;
}

function update() {
	try {

		$sql = "UPDATE my_book SET "
				."created = NOW(),"
				."name = '{$_POST['name']}',"
				."kana = '{$_POST['kana']}',"
				."phone = '{$_POST['phone']}',"
				."email = '{$_POST['email-1']}@{$_POST['email-2']}',"
				."pref = '{$_POST['pref']}',"
				."address = '{$_POST['address']}' "
				."WHERE id = '{$_POST['id']}'";

		$stmt = $GLOBALS['pdo'] -> query($sql);

		publish_after_form();

		if($stmt === FALSE) {
			var_dump('UPDATE ERROR!');
		}	

	} catch (Exception $e) {
		var_dump($e -> getMessage());
	}
	$pdo = null;
}

function publish_edit_html() {
	echo $GLOBALS['html_header'];
echo <<< _HTML_
<body class="add">
	
<div class="container">
	<h1 class="page-header">Address Book</h1>
	<div class="row">
		
		<div class="span9">
			<form action="{$_SERVER['SCRIPT_NAME']}" method="post">
				<input type="hidden" name="id" value="{$GLOBALS['data']['id']}">
				<legend>住所を編集する</legend>
				<div class="control-group">
					<label class="control-label" for="inputEmail">名前: </label>
					<div class="controls">
               			<input type="text" name="name" placeholder="名前" class="input-xlarge" value="{$GLOBALS['data']['name']}">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">ふりがな: </label>
					<div class="controls">
               			<input type="text" name="kana" placeholder="ふりがな" class="input-xlarge" value="{$GLOBALS['data']['kana']}">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">電話番号: </label>
					<div class="controls">
               			<input type="text" name="phone" placeholder="電話番号" class="input-xlarge" value="{$GLOBALS['data']['phone']}">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">メールアドレス: </label>
					<div class="controls">
               			<input type="text" name="email-1" class="input-large" value="{$GLOBALS['email_arr'][0]}">
               			@
               			<input type="text" name="email-2" class="input-xlarge" value="{$GLOBALS['email_arr'][1]}">
              		</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">住所: </label>
					<div class="controls">
               			<input type="text" name="pref" class="input-large" placeholder="県名" value="{$GLOBALS['data']['pref']}">
              		</div>
              		<div class="controls">
               			<input type="text" name="address" class="input-xxlarge" placeholder="以降の住所" value="{$GLOBALS['data']['address']}">
              		</div>
				</div>
				<input class="btn btn-large btn-primary" type="submit" value="登録">
			</form>
		</div><!-- /.span9 -->
	</div><!-- /.row -->
</div>
_HTML_;
	echo $GLOBALS['html_footer'];

}

function publish_after_form() {
	echo $GLOBALS['html_header'];
echo <<< _HTML_
<body class="add">
	
<div class="container">
	<h1 class="page-header">Address Book</h1>
	<div class="row">
		
		<div class="span9">
			<p>アップデート完了</p>
			<p><a class="btn btn-large btn-primary" href="./">もどる</a></p>
		</div><!-- /.span9 -->
		</div><!-- /.span9 -->
	</div><!-- /.row -->
</div>
_HTML_;
	echo $GLOBALS['html_footer'];
}


//トップに戻す
function return_top() {
	header('location:./');
}























