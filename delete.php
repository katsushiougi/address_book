<?php

require_once('./pdo.php');
require_once('./functions.php');

delete();

?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>Address Book</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
</head>
<body class="add">
	
<div class="container">
	<h1 class="page-header">Address Book</h1>
	<div class="row">
		<?php include_once('./include/sidebar.php'); ?>
		<div class="span9">
			<p>削除しました。</p>
			<p><a href="./" class="btn btn-large btn-primary" >もどる</a></p>
		</div><!-- /.span9 -->
	</div><!-- /.row -->
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>